
## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-gogetssl!1

---

## 0.1.1 [12-13-2021]

- Initial Commit

See commit be62faa

---
