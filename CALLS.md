## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for GoGetSSL. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for GoGetSSL.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the GoGetSSL. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAllProducts(authKey, callback)</td>
    <td style="padding:15px">getAllProducts</td>
    <td style="padding:15px">{base_path}/{version}/products?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllProductPrices(authKey, callback)</td>
    <td style="padding:15px">getAllProductPrices</td>
    <td style="padding:15px">{base_path}/{version}/products/all_prices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductDetails(authKey, productId, callback)</td>
    <td style="padding:15px">getProductDetails</td>
    <td style="padding:15px">{base_path}/{version}/products/details/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductPrice(authKey, productId, callback)</td>
    <td style="padding:15px">getProductPrice</td>
    <td style="padding:15px">{base_path}/{version}/products/price/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductAgreement(authKey, productId, callback)</td>
    <td style="padding:15px">getProductAgreement</td>
    <td style="padding:15px">{base_path}/{version}/products/agreement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslProducts(authKey, callback)</td>
    <td style="padding:15px">getSslProducts</td>
    <td style="padding:15px">{base_path}/{version}/products/ssl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslProduct(authKey, productId, callback)</td>
    <td style="padding:15px">getSslProduct</td>
    <td style="padding:15px">{base_path}/{version}/products/ssl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decodeCSR(csr, callback)</td>
    <td style="padding:15px">decodeCSR</td>
    <td style="padding:15px">{base_path}/{version}/tools/csr/decode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCSR(csrCommonname, csrOrganization, csrDepartment, csrCity, csrState, csrCountry, csrEmail, callback)</td>
    <td style="padding:15px">generateCSR</td>
    <td style="padding:15px">{base_path}/{version}/tools/csr/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateCSR(csr, callback)</td>
    <td style="padding:15px">validateCSR</td>
    <td style="padding:15px">{base_path}/{version}/tools/csr/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainAlternative(authKey, csr, callback)</td>
    <td style="padding:15px">getDomainAlternative</td>
    <td style="padding:15px">{base_path}/{version}/tools/domain/alternative?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainEmails(authKey, domain, callback)</td>
    <td style="padding:15px">getDomainEmails</td>
    <td style="padding:15px">{base_path}/{version}/tools/domain/emails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDomainEmailsForGeotrust(authKey, domain, callback)</td>
    <td style="padding:15px">getDomainEmailsForGeotrust</td>
    <td style="padding:15px">{base_path}/{version}/tools/domain/emails/geotrust?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">domainGetFromWhois(authKey, domain, callback)</td>
    <td style="padding:15px">domainGetFromWhois</td>
    <td style="padding:15px">{base_path}/{version}/tools/domain_get_from_whois?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebServers(authKey, supplierId, callback)</td>
    <td style="padding:15px">getWebServers</td>
    <td style="padding:15px">{base_path}/{version}/tools/webservers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountDetails(authKey, callback)</td>
    <td style="padding:15px">getAccountDetails</td>
    <td style="padding:15px">{base_path}/{version}/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountBalance(authKey, callback)</td>
    <td style="padding:15px">getAccountBalance</td>
    <td style="padding:15px">{base_path}/{version}/account/balance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNewLEI(authKey, test, productId, legalName, lastName, isLevel2DataAvailable, incorporationDate, legalState, legalPostal, legalfirstAddressLine, legalCountry, legalCity, multiYearSupport, callback)</td>
    <td style="padding:15px">createNewLEI</td>
    <td style="padding:15px">{base_path}/{version}/orders/lei/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLEIJurisdictions(authKey, callback)</td>
    <td style="padding:15px">getLEIJurisdictions</td>
    <td style="padding:15px">{base_path}/{version}/lei/jurisdictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmLEIDataQuality(authKey, orderId, confirm, callback)</td>
    <td style="padding:15px">confirmLEIDataQuality</td>
    <td style="padding:15px">{base_path}/{version}/lei/confirm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeiStatus(authKey, orderId, callback)</td>
    <td style="padding:15px">getLeiStatus</td>
    <td style="padding:15px">{base_path}/{version}/lei/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewLEI(authKey, orderId, isLevel1DataSame, firstName, lastName, callback)</td>
    <td style="padding:15px">Renew LEI</td>
    <td style="padding:15px">{base_path}/{version}/orders/lei/renew/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">leiLoolup(authKey, query, callback)</td>
    <td style="padding:15px">leiLoolup</td>
    <td style="padding:15px">{base_path}/{version}/lei/lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSLOrder(authKey, productId, csr, serverCount, period, webserverType, adminFirstname, adminLastname, adminPhone, adminTitle, adminEmail, adminCity, adminCountry, adminPostalcode, dcvMethod, techFirstname, techLastname, techPhone, techTitle, techEmail, techAddressline1, adminAddressline1, callback)</td>
    <td style="padding:15px">addSSLOrder</td>
    <td style="padding:15px">{base_path}/{version}/orders/add_ssl_order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reissueSSLOrder(authKey, orderId, webserverType, csr, dcvMethod, dnsNames, approverEmails, approverEmail, signatureHash, uniqueCode, callback)</td>
    <td style="padding:15px">reissueSSLOrder</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/reissue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSLRenewOrder(authKey, productId, csr, serverCount, period, approverEmail, webserverType, adminFirstname, adminLastname, adminPhone, adminTitle, adminEmail, adminCity, adminCountry, adminOrganization, dcvMethod, techFirstname, techLastname, techPhone, techTitle, techEmail, techAddressline1, adminAddressline1, techOrganization, techCity, techCountry, orgDivision, orgAddressline1, orgCity, orgCountry, orgPhone, orgPostalcode, orgRegion, approverEmails, dnsNames, signatureHash, test, callback)</td>
    <td style="padding:15px">addSSLRenewOrder</td>
    <td style="padding:15px">{base_path}/{version}/orders/add_ssl_renew_order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSSLSANOrder(authKey, orderId, count, wildcardSanCount, singleSanCount, callback)</td>
    <td style="padding:15px">addSSLSANOrder</td>
    <td style="padding:15px">{base_path}/{version}/orders/add_ssl_san_order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelOrder(authKey, orderId, reason, callback)</td>
    <td style="padding:15px">cancelOrder</td>
    <td style="padding:15px">{base_path}/{version}/orders/cancel_ssl_order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrdersStatuses(authKey, cids, callback)</td>
    <td style="padding:15px">getOrdersStatuses</td>
    <td style="padding:15px">{base_path}/{version}/orders/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrderStatus(authKey, orderId, callback)</td>
    <td style="padding:15px">getOrderStatus</td>
    <td style="padding:15px">{base_path}/{version}/orders/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">recheckCAA(authKey, orderId, callback)</td>
    <td style="padding:15px">recheck CAA</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/recheck-caa/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnpaidOrders(authKey, callback)</td>
    <td style="padding:15px">getUnpaidOrders</td>
    <td style="padding:15px">{base_path}/{version}/orders/list/unpaid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllSSLOrders(authKey, limit, offset, callback)</td>
    <td style="padding:15px">getAllSSLOrders</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">comodoClaimFreeEV(authKey, orderId, callback)</td>
    <td style="padding:15px">comodoClaimFreeEV</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/comodo_claim_free_ev/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTotalOrders(authKey, callback)</td>
    <td style="padding:15px">getTotalOrders</td>
    <td style="padding:15px">{base_path}/{version}/account/total_orders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeDomainsValidationMethod(authKey, orderId, newMethods, domains, callback)</td>
    <td style="padding:15px">changeDomainsValidationMethod</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/change_domains_validation_method/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeValidationMethod(authKey, orderId, domain, newMethod, callback)</td>
    <td style="padding:15px">changeValidationMethod</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/change_validation_method/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeValidationEmail(authKey, orderId, approverEmail, callback)</td>
    <td style="padding:15px">changeValidationEmail</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/change_validation_email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resendValidationEmail(authKey, orderId, callback)</td>
    <td style="padding:15px">resendValidationEmail</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/resend_validation_email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeDcv(authKey, orderId, domainName, newMethod, callback)</td>
    <td style="padding:15px">changeDcv</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/change_dcv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revalidate(authKey, orderId, callback)</td>
    <td style="padding:15px">revalidate</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/revalidate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resend(authKey, orderId, domain, callback)</td>
    <td style="padding:15px">resend</td>
    <td style="padding:15px">{base_path}/{version}/orders/ssl/resend/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllInvoices(authKey, callback)</td>
    <td style="padding:15px">getAllInvoices</td>
    <td style="padding:15px">{base_path}/{version}/account/invoices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnpaidInvoices(authKey, callback)</td>
    <td style="padding:15px">getUnpaidInvoices</td>
    <td style="padding:15px">{base_path}/{version}/account/invoices/unpaid?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrderInvoice(authKey, orderId, callback)</td>
    <td style="padding:15px">getOrderInvoice</td>
    <td style="padding:15px">{base_path}/{version}/orders/invoice/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
