# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the GoGetSSL System. The API that was used to build the adapter for GoGetSSL is usually available in the report directory of this adapter. The adapter utilizes the GoGetSSL API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The GoGetSSL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GoGetSSL to offer security and trust information. 

With this adapter you have the ability to perform operations with GoGetSSL such as:

- Products
- Validation
- Web Servers

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
