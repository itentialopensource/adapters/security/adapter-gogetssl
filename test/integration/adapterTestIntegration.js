/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-gogetssl',
      type: 'GoGetSSL',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const GoGetSSL = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] GoGetSSL Adapter Test', () => {
  describe('GoGetSSL Class Tests', () => {
    const a = new GoGetSSL(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const productsAuthKey = 'fakedata';
    describe('#getAllProducts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllProducts(productsAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.products));
                assert.equal(false, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getAllProducts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProductAgreement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProductAgreement(productsAuthKey, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('66', data.response.productId);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getProductAgreement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllProductPrices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllProductPrices(productsAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.product_prices));
                assert.equal(false, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getAllProductPrices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const productsProductId = 'fakedata';
    describe('#getProductDetails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getProductDetails(productsAuthKey, productsProductId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getProductDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProductPrice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProductPrice(productsAuthKey, productsProductId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.product_price));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getProductPrice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSslProducts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSslProducts(productsAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.products));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getSslProducts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSslProduct - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSslProduct(productsAuthKey, productsProductId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.product);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Products', 'getSslProduct', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cSRCsr = 'fakedata';
    describe('#decodeCSR - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.decodeCSR(cSRCsr, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.csrResult);
                assert.equal(false, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CSR', 'decodeCSR', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cSRCsrCommonname = 'fakedata';
    const cSRCsrOrganization = 'fakedata';
    const cSRCsrDepartment = 'fakedata';
    const cSRCsrCity = 'fakedata';
    const cSRCsrState = 'fakedata';
    const cSRCsrCountry = 'fakedata';
    const cSRCsrEmail = 'fakedata';
    describe('#generateCSR - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateCSR(cSRCsrCommonname, cSRCsrOrganization, cSRCsrDepartment, cSRCsrCity, cSRCsrState, cSRCsrCountry, cSRCsrEmail, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('-----BEGIN CERTIFICATE REQUEST-----\nMIICyzCCAbMCAQAwgYUxCzAJBgNVBAYTAkxWMRAwDgYDVQQIDAdMYXRnYWxlMRMw\nEQYDVQQHDApEYXVnYXZwaWxzMREwDwYDVQQKDAhPT08gdGVzdDELMAkGA1UECwwC\nSVQxETAPBgNVBAMMCHRlc3QuY29tMRwwGgYJKoZIhvcNAQkBFg1pbmZvQHRlc3Qu\nY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0O5g2LTcHSTWI8Km\nqNO/4bxldmUYW+RdyqvkWNb/Dr0H5plitoTYqlIeSyJFv7LGhnM9ragr+SGh6wYC\nntxOf2VUuxcsZklmXb9iErIGr8nGfmPTNXSYDyBk+et5GeSxhEPUpWzsMviqxg/9\njAg9ncatvaJRiaq1FGv3qyypyX6YnUlamqFy1DuHVykRueTNJWLfslqGiOuvN4/K\n9xJiHMyE9l8rb8saWrw/FubYHMlJ/8XpqUaTIfezbHjkBFTNj8Ieo5OZDB+1QzkE\n9M2SwMembOW0mDBiiUC8tUwKdh9w6L8iB7adfLINpEDupiz2ExkdYA4XPrEj0hom\nI6NXWwIDAQABoAAwDQYJKoZIhvcNAQELBQADggEBAItC8n1XVxryLH0Qoh9BTGAV\nE9I2+peVDdRM/xxRyEw3ykmYQ1iYz0ezIchfjFUl03AiILrE8cJofqfxBxgYU2E5\nH9TyKIxSeo+9LWviqGti/vgV7IdM4DYs03/KTYlWJGX0Sy1xWuvgJcqrLRfvviba\nHtT8g1eVs8YR9dgbjgA7po7RYrux2uf6fIUCk2vrFQZBCaDkm7cbqZtjNNnuId2c\nyiz1eqC4TM7SLkP7ap9N1Muw0+k/9Wzkd5ee+KEpfImMhNmQrpy6b0miEBWJrTyu\nXgtF9dFOSSdETwi/TwbOxLS5VFu/Bg/O7fyMoptSfAV6ud5g/gMU/eW4Lx1eFTg=\n-----END CERTIFICATE REQUEST-----', data.response.csr_code);
                assert.equal('-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDQ7mDYtNwdJNYj\nwqao07/hvGV2ZRhb5F3Kq+RY1v8OvQfmmWK2hNiqUh5LIkW/ssaGcz2tqCv5IaHr\nBgKe3E5/ZVS7FyxmSWZdv2ISsgavycZ+Y9M1dJgPIGT563kZ5LGEQ9SlbOwy+KrG\nD/2MCD2dxq29olGJqrUUa/erLKnJfpidSVqaoXLUO4dXKRG55M0lYt+yWoaI6683\nj8r3EmIczIT2XytvyxpavD8W5tgcyUn/xempRpMh97NseOQEVM2Pwh6jk5kMH7VD\nOQT0zZLAx6Zs5bSYMGKJQLy1TAp2H3DovyIHtp18sg2kQO6mLPYTGR1gDhc+sSPS\nGiYjo1dbAgMBAAECggEBAKX11KQO9hGIYzlAs1XD7dWH6yiioTjNK9uDsv0Gus+g\nRBhemtTUra6NUFbqQHFHtv6xp7q7rUJV/uJLgeVipk/309hRTglqIJAzJ9ol473b\nD3ryHKGHngnV95+19holWQTxRIarAOx0LmLp7MNBAIDFgtjxiMdL/E7efHtSHQRZ\nxZf+E+Gds4MZcJdM6TnxGfarWHXuw5tEWSaIZtWBIvYK5JAhLBY3x9tNPd+IEvgU\nb2apYxtnfe1FCARD2rs0sxSWGV9yGUUnXXtGHO3Z8YPekxTw7P/6Yq7cz9cyeFpL\nb1KVN7SNikJO8DngqaKkoCN0i2frK25CnnNI1R0rreECgYEA7kbXL5gYqITp1tyz\nyKG80E7WZ+KvPOyJnT8cpUaAM+7GDFe3hsFKsVBng2TEvkhjfUaIamGWErhuDx4C\naMoX8ic0G8ZTB5jaFB8bGHPqx9+qI1vVHWcgvhjT8Aw+P2E49EQI+EaGYpkbzLB9\nemoDiM+qUdM8Q0G+DPCzJs8KfxMCgYEA4HjAsVcg+W+khdbVKAOyiU4oY7OpjIoF\nYaGT69HplVhWQtN7Hkv0JoAA6XcIqiaBM/zVZJVdY0RRDb49hVo+hI6zGZDsiGfU\nnrz3rH8K3TkPoT5OflfxTrCcpOgQ/e+EkdaQPz7iwRnF3Pu+y2hcH5MsUJul9N6y\nQJoPulOop5kCgYEA7J9Y7qDCTAfCGGeP+jvzv4GrFftdPsk/V/LmxZKfmfMSpgst\n7BpjzHYmdatKXhSRCEVVDx/d4rVYRpbsmZen9AfOjY6DwdWfK8BALiZfDyIkWXzG\nxK43K9CIF6hw4Ivx/VDa/4M5zQxzOPBDi59f5ysi3qNBAyF0VHFr16xfphUCgYAv\nwMCNeZGkPuHrLxbNmmvC1gO1oiTxpvMaH/uN8WwVv2MqeD765QGdpyGKiS4otGRM\nIP9slHH3ijMMD2qbEeW24Xysda48HoqW8nBmG+i9w0PV86FTM3Y8XUoVsuDahLfZ\nJqItMj2fAtKa/ltKfnsKpI6K33Hgs/eL0rqB0wZQEQKBgEOLZJSkOIFFGqaxvAAZ\naR5mnCYO/CGzupH42KNgyYNinvGV5l16QaaCpNIZcxV3x/HBmcx2Gr+4SIpr4N9j\nJ/oC4WwOoV0nLCYd7ZnznhDBdYvh60oeni7SGhVVgHyUN8+rn1/QxidCHHuZ3nyu\nMqEVYn1vanD47bJoe8pbgj2L\n-----END PRIVATE KEY-----', data.response.csr_key);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CSR', 'generateCSR', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateCSR - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.validateCSR(cSRCsr, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CSR', 'validateCSR', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCVAuthKey = 'fakedata';
    const dCVCsr = 'fakedata';
    describe('#getDomainAlternative - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainAlternative(dCVAuthKey, dCVCsr, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.validation);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCV', 'getDomainAlternative', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dCVDomain = 'fakedata';
    describe('#getDomainEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainEmails(dCVAuthKey, dCVDomain, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.error);
                assert.equal('107', data.response.message);
                assert.equal('Error fetching email list. Not possible to fetch approval emails for domain `example`.', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCV', 'getDomainEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDomainEmailsForGeotrust - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDomainEmailsForGeotrust(dCVAuthKey, dCVDomain, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.GeotrustApprovalEmails));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCV', 'getDomainEmailsForGeotrust', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#domainGetFromWhois - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.domainGetFromWhois(dCVAuthKey, dCVDomain, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.error);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DCV', 'domainGetFromWhois', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webserversAuthKey = 'fakedata';
    const webserversSupplierId = 'fakedata';
    describe('#getWebServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebServers(webserversAuthKey, webserversSupplierId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.webservers));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webservers', 'getWebServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accountAuthKey = 'fakedata';
    describe('#getAccountDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountDetails(accountAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.company_name);
                assert.equal('string', data.response.company_vat);
                assert.equal('string', data.response.company_phone);
                assert.equal('string', data.response.phone);
                assert.equal('string', data.response.fax);
                assert.equal('string', data.response.address);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.postal_code);
                assert.equal('string', data.response.country);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.reseller_plan);
                assert.equal('string', data.response.currency);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'getAccountDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAccountBalance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAccountBalance(accountAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.balance);
                assert.equal('string', data.response.currency);
                assert.equal(false, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Account', 'getAccountBalance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lEIAuthKey = 'fakedata';
    const lEIOrderId = 'fakedata';
    const lEIConfirm = 555;
    describe('#confirmLEIDataQuality - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.confirmLEIDataQuality(lEIAuthKey, lEIOrderId, lEIConfirm, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'confirmLEIDataQuality', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lEITest = 555;
    const lEIProductId = 555;
    const lEILegalName = 'fakedata';
    const lEILastName = 'fakedata';
    const lEIIsLevel2DataAvailable = 555;
    const lEIIncorporationDate = 'fakedata';
    const lEILegalState = 'fakedata';
    const lEILegalPostal = 'fakedata';
    const lEILegalfirstAddressLine = 'fakedata';
    const lEILegalCountry = 'fakedata';
    const lEILegalCity = 'fakedata';
    const lEIMultiYearSupport = 555;
    describe('#createNewLEI - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNewLEI(lEIAuthKey, lEITest, lEIProductId, lEILegalName, lEILastName, lEIIsLevel2DataAvailable, lEIIncorporationDate, lEILegalState, lEILegalPostal, lEILegalfirstAddressLine, lEILegalCountry, lEILegalCity, lEIMultiYearSupport, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.product_id);
                assert.equal('string', data.response.order_number);
                assert.equal('string', data.response.orderId);
                assert.equal('string', data.response.invoice_num);
                assert.equal('string', data.response.invoice_id);
                assert.equal('string', data.response.lei_id);
                assert.equal(7, data.response.order_amount);
                assert.equal('string', data.response.currency);
                assert.equal('string', data.response.tax);
                assert.equal('string', data.response.tax_rate);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'createNewLEI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lEIIsLevel1DataSame = 555;
    const lEIFirstName = 'fakedata';
    describe('#renewLEI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renewLEI(lEIAuthKey, lEIOrderId, lEIIsLevel1DataSame, lEIFirstName, lEILastName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'renewLEI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLEIJurisdictions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLEIJurisdictions(lEIAuthKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'getLEIJurisdictions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const lEIQuery = 'fakedata';
    describe('#leiLoolup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.leiLoolup(lEIAuthKey, lEIQuery, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'leiLoolup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLeiStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLeiStatus(lEIAuthKey, lEIOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.error);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LEI', 'getLeiStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersAuthKey = 'fakedata';
    const ordersCsr = 'fakedata';
    const ordersServerCount = 555;
    const ordersPeriod = 555;
    const ordersWebserverType = 555;
    const ordersAdminFirstname = 'fakedata';
    const ordersAdminLastname = 'fakedata';
    const ordersAdminPhone = 555;
    const ordersAdminTitle = 'fakedata';
    const ordersAdminEmail = 'fakedata';
    const ordersAdminCity = 'fakedata';
    const ordersAdminCountry = 'fakedata';
    const ordersAdminPostalcode = 'fakedata';
    const ordersDcvMethod = 'fakedata';
    const ordersTechFirstname = 'fakedata';
    const ordersTechLastname = 'fakedata';
    const ordersTechPhone = 555;
    const ordersTechTitle = 'fakedata';
    const ordersTechEmail = 'fakedata';
    const ordersTechAddressline1 = 'fakedata';
    const ordersAdminAddressline1 = 'fakedata';
    let ordersOrderId = 'fakedata';
    describe('#addSSLOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSSLOrder(ordersAuthKey, 555, ordersCsr, ordersServerCount, ordersPeriod, ordersWebserverType, ordersAdminFirstname, ordersAdminLastname, ordersAdminPhone, ordersAdminTitle, ordersAdminEmail, ordersAdminCity, ordersAdminCountry, ordersAdminPostalcode, ordersDcvMethod, ordersTechFirstname, ordersTechLastname, ordersTechPhone, ordersTechTitle, ordersTechEmail, ordersTechAddressline1, ordersAdminAddressline1, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('77', data.response.productId);
                assert.equal('object', typeof data.response.approver_method);
                assert.equal(1332864, data.response.orderId);
                assert.equal(908032, data.response.invoice_id);
                assert.equal('active', data.response.order_status);
                assert.equal(true, data.response.success);
                assert.equal(true, Array.isArray(data.response.san));
                assert.equal(67.61, data.response.order_amount);
                assert.equal('EUR', data.response.currency);
                assert.equal('12.64', data.response.tax);
                assert.equal('23%', data.response.tax_rate);
              } else {
                runCommonAsserts(data, error);
              }
              ordersOrderId = data.response.orderId;
              saveMockData('Orders', 'addSSLOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersProductId = 555;
    const ordersApproverEmail = 'fakedata';
    const ordersAdminOrganization = 'fakedata';
    const ordersTechOrganization = 'fakedata';
    const ordersTechCity = 'fakedata';
    const ordersTechCountry = 'fakedata';
    const ordersOrgDivision = 'fakedata';
    const ordersOrgAddressline1 = 'fakedata';
    const ordersOrgCity = 'fakedata';
    const ordersOrgCountry = 'fakedata';
    const ordersOrgPhone = 555;
    const ordersOrgPostalcode = 555;
    const ordersOrgRegion = 'fakedata';
    const ordersApproverEmails = 'fakedata';
    const ordersDnsNames = 'fakedata';
    const ordersSignatureHash = 'fakedata';
    const ordersTest = 'fakedata';
    describe('#addSSLRenewOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addSSLRenewOrder(ordersAuthKey, ordersProductId, ordersCsr, ordersServerCount, ordersPeriod, ordersApproverEmail, ordersWebserverType, ordersAdminFirstname, ordersAdminLastname, ordersAdminPhone, ordersAdminTitle, ordersAdminEmail, ordersAdminCity, ordersAdminCountry, ordersAdminOrganization, ordersDcvMethod, ordersTechFirstname, ordersTechLastname, ordersTechPhone, ordersTechTitle, ordersTechEmail, ordersTechAddressline1, ordersAdminAddressline1, ordersTechOrganization, ordersTechCity, ordersTechCountry, ordersOrgDivision, ordersOrgAddressline1, ordersOrgCity, ordersOrgCountry, ordersOrgPhone, ordersOrgPostalcode, ordersOrgRegion, ordersApproverEmails, ordersDnsNames, ordersSignatureHash, ordersTest, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'addSSLRenewOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersCount = 555;
    const ordersWildcardSanCount = 555;
    const ordersSingleSanCount = 555;
    describe('#addSSLSANOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addSSLSANOrder(ordersAuthKey, ordersOrderId, ordersCount, ordersWildcardSanCount, ordersSingleSanCount, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1146951, data.response.orderId);
                assert.equal(786148, data.response.invoice_id);
                assert.equal('active', data.response.order_status);
                assert.equal(true, data.response.success);
                assert.equal(248.73, data.response.order_amount);
                assert.equal('EUR', data.response.currency);
                assert.equal('0.00', data.response.tax);
                assert.equal('0%', data.response.tax_rate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'addSSLSANOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersReason = 'fakedata';
    describe('#cancelOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelOrder(ordersAuthKey, ordersOrderId, ordersReason, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Cancell request submitted.', data.response.message);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'cancelOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#comodoClaimFreeEV - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.comodoClaimFreeEV(ordersAuthKey, ordersOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.error);
                assert.equal('124', data.response.message);
                assert.equal('Order can\'t be processed with Free EV upgrade, wait for order to become active!', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'comodoClaimFreeEV', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersUniqueCode = 'fakedata';
    describe('#reissueSSLOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reissueSSLOrder(ordersAuthKey, ordersOrderId, ordersWebserverType, ordersCsr, ordersDcvMethod, ordersDnsNames, ordersApproverEmails, ordersApproverEmail, ordersSignatureHash, ordersUniqueCode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'reissueSSLOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ordersCids = 'fakedata';
    describe('#getOrdersStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrdersStatuses(ordersAuthKey, ordersCids, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.certificates));
                assert.equal(true, data.response.success);
                assert.equal(1637157818, data.response.time_stamp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'getOrdersStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTotalOrders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTotalOrders(ordersAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.total_orders);
                assert.equal(false, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'getTotalOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnpaidOrders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnpaidOrders(ordersAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.orders));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'getUnpaidOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllSSLOrders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllSSLOrders(ordersAuthKey, 555, 555, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.orders));
                assert.equal(5, data.response.limit);
                assert.equal(4, data.response.offset);
                assert.equal(2, data.response.count);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'getAllSSLOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#recheckCAA - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.recheckCAA(ordersAuthKey, ordersOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.error);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'recheckCAA', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrderStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrderStatus(ordersAuthKey, ordersOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1146748, data.response.orderId);
                assert.equal(123456789, data.response.partner_order_id);
                assert.equal('S1130555', data.response.internal_id);
                assert.equal('processing', data.response.status);
                assert.equal('', data.response.status_description);
                assert.equal('1', data.response.dcv_status);
                assert.equal('77', data.response.productId);
                assert.equal('my-domain.tld', data.response.domain);
                assert.equal('object', typeof data.response.approver_method);
                assert.equal('my-domain.tld,www.my-domain.tld,my-domain1.tld', data.response.domains);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Orders', 'getOrderStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const validationAuthKey = 'fakedata';
    const validationOrderId = 'fakedata';
    const validationDomainName = 'fakedata';
    const validationNewMethod = 'fakedata';
    describe('#changeDcv - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeDcv(validationAuthKey, validationOrderId, validationDomainName, validationNewMethod, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('63', data.response.product_id);
                assert.equal('object', typeof data.response.validation);
                assert.equal('Validation method has been successfully updated', data.response.success_message);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'changeDcv', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const validationNewMethods = 'fakedata';
    const validationDomains = 'fakedata';
    describe('#changeDomainsValidationMethod - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeDomainsValidationMethod(validationAuthKey, validationOrderId, validationNewMethods, validationDomains, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Change Validation request submitted.', data.response.message);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'changeDomainsValidationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const validationApproverEmail = 'fakedata';
    describe('#changeValidationEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationEmail(validationAuthKey, validationOrderId, validationApproverEmail, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'changeValidationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const validationDomain = 'fakedata';
    describe('#changeValidationMethod - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationMethod(validationAuthKey, validationOrderId, validationDomain, validationNewMethod, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-gogetssl-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'changeValidationMethod', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resend(validationAuthKey, validationOrderId, validationDomain, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.error);
                assert.equal('Resend error', data.response.message);
                assert.equal('Resend can be call only for Email validation type.', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'resend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revalidate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.revalidate(validationAuthKey, validationOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.error);
                assert.equal('Domain error', data.response.message);
                assert.equal('This domain name not found in this certificate.', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'revalidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resendValidationEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resendValidationEmail(validationAuthKey, validationOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('63', data.response.product_id);
                assert.equal('Validation email has been successfully sent', data.response.message);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Validation', 'resendValidationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const invoicesAuthKey = 'fakedata';
    describe('#getAllInvoices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllInvoices(invoicesAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.invoices));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getAllInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUnpaidInvoices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUnpaidInvoices(invoicesAuthKey, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.invoices));
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getUnpaidInvoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrderInvoice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrderInvoice(invoicesAuthKey, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1146748', data.response.orderId);
                assert.equal('GGS-0719723755', data.response.number);
                assert.equal('2019-07-23', data.response.date);
                assert.equal('59.13', data.response.subtotal);
                assert.equal('12.42', data.response.tax);
                assert.equal('21.00', data.response.tax_rate);
                assert.equal('71.55', data.response.total);
                assert.equal('paid', data.response.status);
                assert.equal('EUR', data.response.currency);
                assert.equal('balance', data.response.payment_method);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Invoices', 'getOrderInvoice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
