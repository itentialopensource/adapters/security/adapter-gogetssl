# GoGetSSL

Vendor: GoGetSSL
Homepage: https://GoGetSSL.com/

Product: GoGetSSL
Product Page: https://GoGetSSL.com/

## Introduction
We classify GoGetSSL into the Security (SASE) domain as GoGetSSL provides Security and Trust solutions. 

"GoGetSSL® offers fastest issuance of SSL due to use of LEI code and API automation." 

## Why Integrate
The GoGetSSL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with GoGetSSL to offer security and trust information. 

With this adapter you have the ability to perform operations with GoGetSSL such as:

- Products
- Validation
- Web Servers

## Additional Product Documentation
The [GoGetSSL API Documentation](https://documenter.getpostman.com/view/5593375/T1LHFp7m)
