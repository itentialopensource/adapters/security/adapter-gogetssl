
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:14PM

See merge request itentialopensource/adapters/adapter-gogetssl!12

---

## 0.4.3 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-gogetssl!10

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_18:24PM

See merge request itentialopensource/adapters/adapter-gogetssl!9

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:37PM

See merge request itentialopensource/adapters/adapter-gogetssl!8

---

## 0.4.0 [05-14-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-gogetssl!7

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:42PM

See merge request itentialopensource/adapters/security/adapter-gogetssl!6

---

## 0.3.3 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/security/adapter-gogetssl!5

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:14PM

See merge request itentialopensource/adapters/security/adapter-gogetssl!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:48AM

See merge request itentialopensource/adapters/security/adapter-gogetssl!3

---

## 0.3.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-gogetssl!2

---
